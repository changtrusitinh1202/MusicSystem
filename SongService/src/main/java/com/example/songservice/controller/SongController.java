package com.example.songservice.controller;

import com.example.songservice.config.RestTemplateConfig;
import com.example.songservice.entity.Song;
import com.example.songservice.service.SongService;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jmx.export.UnableToRegisterMBeanException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/songs")
public class SongController {
    @Autowired
    private SongService songService;

    @Autowired
    private RestTemplateConfig restTemplate;

    @Autowired
    public SongController(SongService songService, RestTemplateConfig restTemplate){
        this.songService = songService;
        this.restTemplate = restTemplate;
    }


    private final String SONG_SERVICE = "songService";
    @GetMapping
    @RateLimiter(name = SONG_SERVICE)
    public List<Song> getAllSongs() {
        return songService.getAllSongs();
    }

    @GetMapping("/artists")
    @CircuitBreaker(name = SONG_SERVICE, fallbackMethod = "serviceSongFallBack")
    public Object getForObjectArtist(){
        String url = "http://localhost:8081/artists";
        return restTemplate.getForObject(url, Object.class);
    }

    public String serviceSongFallBack(Exception e){
        return "This is a fall back for Service Song";
    }


    @GetMapping("/{id}")
    @Cacheable(value = "song", key = "#id")
    public Song getSongById(@PathVariable Long id) {
        return songService.getSongById(id);
    }

    @PostMapping
    @CacheEvict(value = "song", allEntries = true) // Xóa tất cả các mục trong cache 'song' khi tạo mới
    public Song createSong(@RequestBody Song song) {
        return songService.createSong(song);
    }

    @DeleteMapping("/{id}")
    @CacheEvict(value = "song", key = "#id") // Xóa mục cache cụ thể
    public void deleteSong(@PathVariable Long id) {
        songService.deleteSong(id);
    }
}

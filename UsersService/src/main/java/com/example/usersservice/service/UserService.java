package com.example.usersservice.service;

import com.example.usersservice.entity.Users;
import org.apache.catalina.User;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface UserService {
     Users findById(Long id);
     Users createUser(Users users);

}

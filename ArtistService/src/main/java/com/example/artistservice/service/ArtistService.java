package com.example.artistservice.service;

import com.example.artistservice.entity.Artist;
import com.example.artistservice.repository.ArtistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ArtistService {
    @Autowired
    private ArtistRepository artistRepository;

    public List<Artist> getAllArtists() {
        return artistRepository.findAll();
    }

    public Artist getArtistById(Long id) {
        return artistRepository.findById(id).orElse(null);
    }

    public Artist createArtist(Artist artist) {
        return artistRepository.save(artist);
    }

    public void deleteArtist(Long id) {
        artistRepository.deleteById(id);
    }

    public Artist updateArtist(Long id, Artist artistDetails){
        Optional<Artist> optionalArtist = artistRepository.findById(id);

        if (optionalArtist.isPresent()) {
            Artist existingArtist = optionalArtist.get();
            existingArtist.setName(artistDetails.getName());
            existingArtist.setGenre(artistDetails.getGenre());
            return artistRepository.save(existingArtist);
        } else {
            return null;
        }
    }
}
